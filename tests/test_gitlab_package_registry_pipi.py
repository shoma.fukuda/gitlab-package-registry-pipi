import pytest
import gitlab_package_registry_pipi.helloworld

class TestStringMethods():

    def test_add(self):
        theSum = gitlab_package_registry_pipi.helloworld.add(2,3)
        assert theSum == 5

    def test_sayHello(self):
        greetings = gitlab_package_registry_pipi.helloworld.sayHello()
        assert greetings == "Hello world!"
